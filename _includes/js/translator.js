downloadButtonElem = document.querySelector("#tl-download-button");
downloadButtonElem.addEventListener("click", handleDownloadButton);

function handleDownloadButton(event) {
	const translations = getTranslations();
	
	if (!translations || translations.length === 0) {
		alert("No translations written, nothing to save");
	} else {
		const currentDate = new Date();
		const fileContent = buildFileContent(translations, currentDate);
		const fileName = `tl-${Date.now()}.yml`;
		saveFile(fileName, fileContent);
	}
}

function getTranslations() {
	const inputBoxElems = document.querySelectorAll(".tl-input-box");
	
	const translations = [];
	
	inputBoxElems.forEach(inputBoxElem => {
		const content = inputBoxElem.value.trim();
		if (content) {
			const textId = inputBoxElem.parentElement.id;
			translations.push({
				id: textId,
				text: content,
			});
		}
	});
	
	return translations;
}

function formatText(text) {
	let formatted = "";
	
	const lines = text.split(/\r?\n|\r|\n/g);
	lines.forEach(line => {
		formatted += `    ${line}\n`;
	});
	
	return formatted;
}

function buildFileContent(translations, currentDate) {
	let content = "";
	
	content += `date: ${currentDate.toISOString()}\n`;
	
	content += "translations:\n";
	translations.forEach(translation => {
		content += `  ${translation.id}: |\n`;
		const formattedText = formatText(translation.text);
		content += `${formattedText}`;
	});
	
	return content;
}

function saveFile(fileName, content) {
	const downloadElem = document.createElement('a');
	const encodedContent = encodeURIComponent(content);
	downloadElem.setAttribute("href", "data:text/plain;charset=utf-8," + encodedContent);
	downloadElem.setAttribute("download", fileName);
	
	downloadElem.style.display = "none";
	document.body.appendChild(downloadElem);
	downloadElem.click();
	document.body.removeChild(downloadElem);
}
