// This is a place best left unseen.
const chaptersList = Object.keys(chapters);
const episodesList = Object.keys(episodes);

const tocHeaderElem = document.createElement("header");
tocHeaderElem.id = "toc-header";
const tocNavElem = document.createElement("nav");
tocNavElem.id = "toc-nav";
const tocNavTopRowElem = document.createElement("div");
tocNavTopRowElem.classList.add("toc-nav-group");
const tocNavBotRowElem = document.createElement("div");
tocNavBotRowElem.classList.add("toc-nav-group");

const anchorObserver = new IntersectionObserver(
	handleAnchorIntersection,
	{ rootMargin: "-10% 0% -90% 0%", threshold: 1 },
);

const chapterSelectElem = createSelectElem("Chapter", "toc-chapter-select", tocNavTopRowElem);
const episodeSelectElem = createSelectElem("Episode", "toc-episode-select", tocNavTopRowElem);
const missionSelectElem = createSelectElem("Mission", "toc-mission-select", tocNavBotRowElem);
const sectionSelectElem = createSelectElem("Section", "toc-section-select", tocNavBotRowElem);

tocNavElem.append(tocNavTopRowElem);
tocNavElem.append(tocNavBotRowElem);

observeAnchorIds(anchorObserver, chaptersList);
observeAnchorIds(anchorObserver, episodesList);
/*
populateSelect(chapterSelectElem, chapters);
populateSelect(episodeSelectElem, episodes);
*/

let chapterSelectPopulated = false;
let episodeSelectPopulated = false;

// 		const defaultChapterId = chapterSelectElem.value;
// 		const defaultEpisodeId = episodeSelectElem.value;
// 		updateMissionSelect(defaultChapterId, defaultEpisodeId);
// 		const defaultMissionId = missionSelectElem.value;
// 		updateSectionSelect(defaultChapterId, defaultEpisodeId, defaultMissionId);

function observeAnchorIds(observer, anchorIds) {
	anchorIds.forEach(id => {
		const anchorElem = document.querySelector(`#${id}`);
		observer.observe(anchorElem);
	});
}

function unobserveAnchorIds(observer, anchorIds) {
	anchorIds.forEach(id => {
		const anchorElem = document.querySelector(`#${id}`);
		observer.unobserve(anchorElem);
	});
}

function firstEpisodeOfChapter(chapterId) {
	const episodesForChapter = toc[chapterId];
	// TODO: keys doesn't preserve order!!
	// FIXME: border between map tutorial and 21-23 broken!
	const firstEpisode = Object.keys(episodesForChapter)[0];
	return `e${firstEpisode.toLowerCase()}`;
}

function chapterForEpisode(episodeId) {
	const episodeText = episodes[episodeId];
	return chaptersList.find(chapter => {
		const episodesForChapter = Object.keys(toc[chapter]);
		return episodesForChapter.includes(episodeText);
	});
}

function episodeForMission(missionId) {
	const idSegments = missionId.split("-");
	const episodeId = idSegments[0];
	return episodeId;
}

function missionNumFromId(missionId) {
	const idSegments = missionId.split("-");
	const missionNum = idSegments[1].slice(1);
	return (missionNum == "map") ? "Map" : missionNum;
}

function updateMissionSelect(chapterId, episodeId) {
	const prevOptionElems = Array.from(missionSelectElem.children);
	if (prevOptionElems.length > 0) {
		const prevAnchorIds = valuesFromOptions(prevOptionElems);
		unobserveAnchorIds(anchorObserver, prevAnchorIds);
	}
	
	const episodeText = episodes[episodeId];
	const missions = Object.keys(toc[chapterId][episodeText]);
	const missionOptions = {};
	const missionIds = [];
	missions.forEach(missionNum => {
		const missionId = `${episodeId}-m${missionNum.toLowerCase()}`;
		missionOptions[missionId] = missionNum;
		missionIds.push(missionId);
	});
	
	observeAnchorIds(anchorObserver, missionIds);
	
	populateSelect(missionSelectElem, missionOptions);
	
	return missionSelectElem.value;
}

function updateSectionSelect(chapterId, episodeId, missionId) {
	const prevOptionElems = Array.from(sectionSelectElem.children);
	if (prevOptionElems.length > 0) {
		const prevAnchorIds = valuesFromOptions(prevOptionElems);
		unobserveAnchorIds(anchorObserver, prevAnchorIds);
	}
	
	const episodeText = episodes[episodeId];
	const missionNum = missionNumFromId(missionId);
	
	const sections = toc[chapterId][episodeText][missionNum];
	
	const sectionOptions = {};
	const sectionIds = [];
	sections.forEach(section => {
		const sectionId = `${episodeId}-m${missionNum.toLowerCase()}-${section.toLowerCase()}`;
		sectionOptions[sectionId] = sectionSubstMap[section];
		sectionIds.push(sectionId);
	});
	
	observeAnchorIds(anchorObserver, sectionIds);
	
	populateSelect(sectionSelectElem, sectionOptions);
}


function valuesFromOptions(optionElems) {
	return optionElems.map(optionElem => (
		optionElem.value
	));
}

function handleSelectEvent(e) {
	const selectElem = e.target;
	location.hash = `#${selectElem.value}`;
	switch (selectElem.id) {
		case "toc-chapter-select": {
			const chapterId = selectElem.value;
			const firstEpisodeId = firstEpisodeOfChapter(chapterId);
			episodeSelectElem.value = firstEpisodeId;
			const newMissionId = updateMissionSelect(chapterId, firstEpisodeId);
			updateSectionSelect(chapterId, firstEpisodeId, newMissionId);
			break;
		}
		case "toc-episode-select": {
			const episodeId = selectElem.value;
			const chapterId = chapterForEpisode(episodeId);
			chapterSelectElem.value = chapterId;
			const newMissionId = updateMissionSelect(chapterId, episodeId);
			updateSectionSelect(chapterId, episodeId, newMissionId);
			break;
		}
		case "toc-mission-select": {
			const missionId = selectElem.value;
			const episodeId = episodeForMission(missionId);
			const chapterId = chapterForEpisode(episodeId);
			const newMissionId = updateSectionSelect(chapterId, episodeId, missionId);
			updateSectionSelect(chapterId, episodeId, newMissionId);
			break;
		}
		case "toc-section-select":
			// nothing
			break;
	}
}

function createSelectElem(label, id, container) {
	wrapperElem = document.createElement("div");
	wrapperElem.classList.add("toc-nav-select");
	
	labelElem = document.createElement("label");
	labelElem.innerHTML = `${label}:`;
	labelElem.htmlFor = id;
	wrapperElem.append(labelElem);
	
	selectElem = document.createElement("select");
	selectElem.id = id;
	selectElem.class = "toc-select";
	selectElem.addEventListener("change", handleSelectEvent);
	wrapperElem.append(selectElem);
	
	container.append(wrapperElem);
	return selectElem;
}

function populateSelect(selectElem, options) {
	optionElems = Object.keys(options).map(id => {
		const label = options[id];
		const optionElem = document.createElement("option");
		optionElem.value = id;
		optionElem.innerHTML = label;
		return optionElem;
	});
	selectElem.replaceChildren(...optionElems);
}

const typeToPriorityMap = {
	"chapter": 0,
	"episode": 1,
	"mission": 2,
	"section": 3,
}

function handleAnchorIntersection(entries, o) {
	const intersectingEntries = entries.filter(entry => (
		entry.isIntersecting
	));
	const intersectingPriorities = intersectingEntries.map(entry => (
		typeToPriorityMap[entry.target.getAttribute("data-type")]
	));
	const maxPriority = Math.max(...intersectingPriorities);
	intersectingEntries.forEach(entry => {
		const anchorElem = entry.target;
		const anchorType = anchorElem.getAttribute("data-type");
		const priority = typeToPriorityMap[entry.target.getAttribute("data-type")];
		// TODO: only process most nested for performance
		if (priority == maxPriority || !chapterSelectPopulated || !episodeSelectPopulated) {
			switch (anchorType) {
				case "chapter": {
					console.log("a");
					const chapterId = anchorElem.id;
					if (!chapterSelectPopulated) {
						populateSelect(chapterSelectElem, chapters);
						chapterSelectPopulated = true;
					}
					chapterSelectElem.value = chapterId;
					const firstEpisodeId = firstEpisodeOfChapter(chapterId);
					episodeSelectElem.value = firstEpisodeId;
					const newMissionId = updateMissionSelect(chapterId, firstEpisodeId);
					updateSectionSelect(chapterId, firstEpisodeId, newMissionId);
					break;
				}
				case "episode": {
					console.log("b");
					const episodeId = anchorElem.id;
					if (!episodeSelectPopulated) {
						populateSelect(episodeSelectElem, episodes);
						episodeSelectPopulated = true;
					}
					episodeSelectElem.value = episodeId;
					const chapterId = chapterForEpisode(episodeId);
					chapterSelectElem.value = chapterId;
					const newMissionId = updateMissionSelect(chapterId, episodeId);
					updateSectionSelect(chapterId, episodeId, newMissionId);
					break;
				}
				case "mission":
					console.log("c");
					const missionId = anchorElem.id;
					missionSelectElem.value = missionId;
					const episodeId = episodeForMission(missionId);
					const chapterId = chapterForEpisode(episodeId);
					updateSectionSelect(chapterId, episodeId, missionId);
					break;
				case "section":
					console.log("d");
					const sectionId = anchorElem.id;
					sectionSelectElem.value = sectionId;
					break;
			}
		}
	});
}

tocHeaderElem.append(tocNavElem);
const bodyElem = document.querySelector("body");
bodyElem.prepend(tocHeaderElem);
