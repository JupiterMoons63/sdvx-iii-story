Welcome to translator mode!
Thank you for helping to make this story accessible to all.

This is currently very barebones but should be functional.  
If you encounter any problems, please report them to the
[issue tracker]({{ site.repo_url | append: '-/issues' }})
for this site's repository.

**Machine translations will not be accepted.**  
You may use machine translation to assist you but please keep it minimal.

**Be interpretive rather than literal with translations.**  
Prioritize readability and enjoyment rather than 100% accuracy.  
Don't keep awkward direct translations, take its meaning and rework it.
Apply contextual knowledge when possible.

**Please use a landscape display orientation.**  
This layout is not yet optimized for smaller-width displays.

**This mode only supports dialogue translation. For other translations, please
edit the substitution tables in the repository.**

**Translation notes are currently not supported.**  
Support coming soon&trade;

Here's a quick tutorial for how to use the translator mode:
- In each row, there is the original untranslated text,
  an existing translation (if there is one), and a text box to fill in a new translation.
- To create a translation for a piece of dialogue, simply write it into the corresponding
  text box.
- Translations are **line-break sensitive**.
  - Try to keep line lengths within range of the originals.
    If the translation for a single line is too long, break it across multiple lines.
  - Automatic word-wrapping will not create a line break, you must use the enter/return key.
  - Use your best judgment for how to divide the translation between lines.
    Try to follow the original when possible.
- New translations will override old translations, so if an existing translation has
  errors or is of poor quality, it can be updated.
- To save your scroll position on the page, click the text ID link for a permalink to
  that piece of dialogue. This will allow you to return later without having to find
  where you left off.
- To save your translations, click the "Save translation file" button at the top of the
  screen.
  - **This page will not save content if you close or refresh the tab. You must
    save your work to a file.**
  - The translation files are made to support sporadic translation sessions. Don't worry
    about keeping everything in a single file.

To submit your translations to the site, do one of the following:
- If you have my contact, you can send the translation files directly to me.
  (Not preferred, but easier for you)
- Submit a merge request
  - [Create a fork]({{ site.repo_url | append: "-/forks/new" }}) of the site repository
    (you only need to do this once)
  - Add the translation files to `_story/translations/{{ page.lang }}/`
  - Commit the changes
  - [Create a merge request]({{ site.repo_url | append: "-/merge_requests/new" }}) to
    merge your changes into the site repository.

Please note that the existing translations on this page will not be updated until they
are merged into the main site.
