---
layout: home
---

# SDVX III Mission Mode Story

This site is still a work-in-progress! Please excuse the rough edges for the
time being.

**Languages:**
- [日本語](./jp) (Japanese)
- [English](./en) (NOT FINISHED YET)

**Translator Mode:**
Help contribute translations for the story!
To coordinate adding new languages, please open an issue in this site's
[GitLab Repository]({{ site.repo_url }})
- [English](./tl-en)
