#!/usr/bin/env ruby

require "./util"
require "./localization"

XML_FILE, LANG, FALLBACK_LANG = Util.parse_args("generate_toc")

base_xml = Util.read_xml(XML_FILE)
toc_html = Util.new_html_fragment

LOCALIZATION = Localization.new(LANG, FALLBACK_LANG)

def new_toc_item(link_text, link_href_id, toc_html)
	toc_item_elem = Util.new_node("li", toc_html)
	toc_link_elem = Util.new_node("a", toc_html, href: "##{link_href_id}")
	toc_link_elem.content = link_text
	toc_item_elem.add_child(toc_link_elem)
	return toc_item_elem
end

toc_elem = Util.new_node("ol", toc_html, id: "toc")

base_xml.css("story > chapter").each do |chapter_xml|
	
	chapter_id = chapter_xml["id"]
	chapter_name = LOCALIZATION.get_subst(:chapters, chapter_id)
	
	toc_chapter_elem = new_toc_item(chapter_name, chapter_id, toc_html)
	
	toc_episode_list_elem = Util.new_node("ol", toc_html)
	chapter_xml.css("> episode, map").each do |episode_xml|
		
		episode_id = episode_xml["id"]
		episode_name = LOCALIZATION.get_subst(:episodes, episode_id)
		
		toc_episode_elem = new_toc_item(episode_name, episode_id, toc_html)
		
		is_map = episode_xml.node_name == "map"
		
		unless is_map
			toc_mission_list_elem = Util.new_node("ol", toc_html)
			episode_xml.css("> mission").each do |mission_xml|
				
				mission_name = mission_xml["name"] # no substitutions
				mission_id = mission_xml["id"]
				
				mission_localization = LOCALIZATION.get("mission",
					capitalize: true,
					space_after: true
				)
				mission_content = mission_localization + mission_name
				
				toc_mission_elem = new_toc_item(mission_content, mission_id, toc_html)
				
				toc_section_list_elem = Util.new_node("ol", toc_html)
				mission_xml.css("> section").each do |section_xml|
					
					section_part = LOCALIZATION.get_subst(:sections, section_xml["status"])
					section_id = section_xml["id"]
					
					toc_section_elem = new_toc_item(section_part, section_id, toc_html)
					
					toc_section_list_elem.add_child(toc_section_elem)
				end
				
				toc_mission_elem.add_child(toc_section_list_elem)
				toc_mission_list_elem.add_child(toc_mission_elem)
			end
			
			toc_episode_elem.add_child(toc_mission_list_elem)
		end
		
		toc_episode_list_elem.add_child(toc_episode_elem)
	end
	
	toc_chapter_elem.add_child(toc_episode_list_elem)
	toc_elem.add_child(toc_chapter_elem)
end

toc_html.add_child(toc_elem)

Util.write_html("toc.html", LANG, toc_html)
