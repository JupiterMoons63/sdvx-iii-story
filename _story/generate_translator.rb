#!/usr/bin/env ruby

require "./util"
require "./localization"
require "./translation_loader"

XML_FILE, LANG, FALLBACK_LANG = Util.parse_args("generate_translator") # eh

base_xml = Util.read_xml(XML_FILE)
translator_html = Util.new_html_fragment

LOCALIZATION = Localization.new(LANG, FALLBACK_LANG)
TRANSLATION  = TranslationLoader.new(LANG)

progress_elem = Util.new_node("div", translator_html, id: "tl-progress")
translator_html.add_child(progress_elem) # content filled in later...

warnings_elem = Util.new_node("div", translator_html, class: "tl-warnings")
LOCALIZATION.get_warnings.each do |warning|
	warning_elem = Util.new_node("p", translator_html)
	warning_elem.content = "warning: #{warning}, please update the substitution tables!"
	warnings_elem.add_child(warning_elem)
end
translator_html.add_child(warnings_elem)

PROGRESS = {
	translated: 0.0,
	total: 0.0,
}

def add_translator_elems(parent, source_xml, translator_html)
	open_paren = LOCALIZATION.get("open_paren")
	close_paren = LOCALIZATION.get("close_paren")
	
	source_xml.css("> character").each do |script_xml|
		
		character_xml_name = script_xml["name"]
		character = LOCALIZATION.get_character(character_xml_name)
		character_name = character[:name]
		character_variant = character[:variant]
		character_tl_note = character[:tl_note]
		
		script_xml.css("> text").each do |text_xml|
			expression = text_xml["expression"]
			text_id = text_xml["id"]
			text_content = text_xml.children
			
			translator_elem = Util.new_node("p", translator_html,
				class: "tl-dialogue",
				id: text_id
			)
			
			id_elem = Util.new_node("a", translator_html,
				class: "tl-id",
				href: "##{text_id}",
			)
			id_elem.content = text_id
			translator_elem.add_child(id_elem)
			
			character_elem = Util.new_node("span", translator_html, class: "tl-character")
			
			name_elem = Util.new_node("span", translator_html, class: "tl-name")
			name_elem.content = character_name
			character_elem.add_child(name_elem)
			
			if character_variant
				variant_elem = Util.new_node("span", translator_html, class: "tl-variant")
				variant_elem.content =
					open_paren +
					character_variant +
					close_paren
				character_elem.add_child(variant_elem)
			end
			
			if expression
				expression_elem = Util.new_node("span", translator_html, class: "tl-expression")
				expression_text = LOCALIZATION.get_subst(:expressions, expression)
				expression_elem.content =
					open_paren +
					expression_text +
					close_paren
				character_elem.add_child(expression_elem)
			end
			
			translator_elem.add_child(character_elem)
			
			text_elem = Util.new_node("span", translator_html, class: "tl-text-original")
			text_elem.add_child(text_content)
			translator_elem.add_child(text_elem)
			
			PROGRESS[:total] += 1
			translation_content = TRANSLATION.get(text_id)
			if translation_content
				PROGRESS[:translated] += 1
			else
				translation_content = "(No translation entry)"
			end
			
			translation_elem = Util.new_node("span", translator_html, class: "tl-text-translation")
			translation_elem.add_child(translation_content)
			translator_elem.add_child(translation_elem)
			
			input_box_elem = Util.new_node("textarea", translator_html,
				class: "tl-input-box",
				rows: 4,
			)
			translator_elem.add_child(input_box_elem)
			
			translator_html.add_child(translator_elem)
		end
	end
end

base_xml.css("story > chapter").each do |chapter_xml|
	
	# chapter_elem = Util.new_node("article", translator_html, class: "tl-chapter") # TODO: not article?
	
	chapter_xml.css("> episode, map").each do |episode_xml|
		
		is_map = episode_xml.node_name == "map"
		# episode_class = is_map ? "tl-map" : "tl-episode"
		
		# episode_elem = Util.new_node("section", translator_html, class: episode_class)
		
		if is_map
			add_translator_elems(translator_html, episode_xml, translator_html)
		else
			episode_xml.css("> mission").each do |mission_xml|
				
				# mission_elem = Util.new_node("mission", translator_html, class: "tl-mission")
				
				mission_xml.css("> section").each do |section_xml|
					
					# section_elem = Util.new_node("section", translator_html, class: "tl-section")
					add_translator_elems(translator_html, section_xml, translator_html)
					
					# mission_elem.add_child(section_elem)
				end
				
				# episode_elem.add_child(mission_elem)
			end
		end
		
		# chapter_elem.add_child(episode_elem)
	end
	
	# translator_html.add_child(chapter_elem)
end

progress = PROGRESS[:translated] / PROGRESS[:total]
raise "translation progress over 100% ??" if progress > 1
percent = (progress * 100).round(2);
progress_elem.content = "Dialogue translation progress: #{percent}%"

Util.write_html("translator.html", "tl-#{LANG}", translator_html)
