#!/bin/bash

LANG_COMBOS=(
	"jp en" # japanese with english fallback
	"en jp" # english with japanese fallback
)

for combo in "${LANG_COMBOS[@]}"
do
	echo "---Generating for language combo: ${combo}"
	./generate_story.rb base.xml $combo
	./generate_toc.rb   base.xml $combo
done

TRANSLATION_COMBOS=(
	"en jp"
)

for combo in "${TRANSLATION_COMBOS[@]}"
do
	echo "---Generating translator mode for combo: ${combo}"
	./generate_translator.rb base.xml $combo
done
