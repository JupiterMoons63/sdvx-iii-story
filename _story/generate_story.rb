#!/usr/bin/env ruby

require "./util"
require "./localization"
require "./translation_loader"

XML_FILE, LANG, FALLBACK_LANG = Util.parse_args("generate_story")

base_xml = Util.read_xml(XML_FILE)
story_html = Util.new_html_fragment

LOCALIZATION = Localization.new(LANG, FALLBACK_LANG)
TRANSLATION  = TranslationLoader.new(LANG)

def add_heading_with_anchor(parent, story_html, params={})
	unless Util.all_keys_exist?([:heading_type, :content, :anchor_id, :anchor_type], params)
		raise "missing required key for add_heading_with_anchor"
	end
	
	heading_elem = Util.new_node(params[:heading_type], story_html)
	heading_elem.content = params[:content]
	parent.add_child(heading_elem)
	
	anchor_elem = Util.new_node("a", story_html,
		id: params[:anchor_id],
		"data-type": params[:anchor_type],
		"aria-hidden": "true"
	)
	parent.add_child(anchor_elem)
end

def add_dialogue_elems(parent, source_xml, story_html)
	source_xml.css("> *").each do |script_xml|
		script_elem = Util.new_node("p", story_html)
		
		case script_xml.node_name
		when "cue"
			cue_type     = script_xml["type"]
			cue_bg       = script_xml["bg"]
			cue_location = script_xml["location"]
			
			open_paren_localization = LOCALIZATION.get("open_paren",
				space_before: true,
				manual_spaces: true
			)
			open_paren = open_paren_localization[:text]
			optional_space = open_paren_localization[:space_before]
			close_paren = LOCALIZATION.get("close_paren")
			background = LOCALIZATION.get("background")
			location = LOCALIZATION.get("location")
			colon = LOCALIZATION.get("colon", space_after: true)
			
			cue_wrapper_elem = Util.new_node("span", story_html, class: "cue-wrapper")
			cue_elem = Util.new_node("span", story_html, class: "cue")
			
			cue_text = nil
			# special cases
			if cue_type == "character-exit"
				character = LOCALIZATION.get_character(script_xml.content.to_s)
				exit_verb = LOCALIZATION.get("exit_verb", space_before: true)
				cue_text = "#{character["name"]}#{exit_verb}"
				cue_elem.content = cue_text
			else
				cue_type_elem = Util.new_node("span", story_html, class: "cue-type")
				cue_type_text = LOCALIZATION.get_subst(:cues, cue_type)
				cue_type_elem.content = cue_type_text
				cue_elem.add_child(cue_type_elem)
			end
			
			if cue_bg
				cue_elem.add_child(optional_space)
				
				bg_text = LOCALIZATION.get_subst(:backgrounds, cue_bg)
				bg_elem = Util.new_node("span", story_html, class: "cue-bg")
				
				bg_elem.content =
					open_paren +
					background +
					colon +
					bg_text +
					close_paren
				
				cue_elem.add_child(bg_elem)
			end
			
			if cue_location
				cue_elem.add_child(optional_space)
				
				location_text = cue_location # TODO: locations table
				location_elem = Util.new_node("span", story_html, class: "cue-location")
				
				location_elem.content =
					open_paren +
					location +
					colon +
					location_text +
					close_paren
				
				cue_elem.add_child(location_elem)
			end
			
			cue_wrapper_elem.add_child(cue_elem)
			script_elem.add_child(cue_wrapper_elem)
		when "character"
			character_xml_name = script_xml["name"]
			character = LOCALIZATION.get_character(character_xml_name)
			character_name = character[:name]
			character_variant = character[:variant]
			character_tl_note = character[:tl_note]
			
			open_paren_localization = LOCALIZATION.get("open_paren",
				space_before: true,
				manual_spaces: true
			)
			open_paren = open_paren_localization[:text]
			optional_space = open_paren_localization[:space_before]
			close_paren = LOCALIZATION.get("close_paren")
			colon = LOCALIZATION.get("colon")
			cont = LOCALIZATION.get("cont")
			anchor_label = LOCALIZATION.get("text_anchor_label")
			
			is_cont = false
			
			script_xml.css("> text").each do |text_xml|
				expression = text_xml["expression"]
				text_id = text_xml["id"]
				text_content = TRANSLATION.get(text_id) || text_xml.children
				
				script_elem["id"] = text_id
				
				character_wrapper_elem = Util.new_node("span", story_html, class: "character-wrapper")
				character_elem = Util.new_node("span", story_html, class: "character")
				
				name_elem = Util.new_node("span", story_html, class: "character-name")
				name_elem.content = character_name
				character_elem.add_child(name_elem)
				
				if character_variant
					character_elem.add_child(optional_space)
					
					variant_elem = Util.new_node("span", story_html, class: "character-variant")
					variant_elem.content =
						open_paren +
						character_variant +
						close_paren
					
					character_elem.add_child(variant_elem)
				end
				
				if is_cont
					character_elem.add_child(optional_space)
					
					cont_elem = Util.new_node("span", story_html, class: "cont")
					cont_elem.content =
						open_paren +
						cont +
						close_paren
					
					character_elem.add_child(cont_elem)
				end
				
				if expression
					character_elem.add_child(optional_space)
					
					expression_text = LOCALIZATION.get_subst(:expressions, expression)
					expression_elem = Util.new_node("span", story_html, class: "expression")
					expression_elem.content =
						open_paren +
						expression_text +
						close_paren
					
					character_elem.add_child(expression_elem)
				end
				
				character_elem.add_child(colon)
				character_elem.add_child("<br/>")
				
				character_wrapper_elem.add_child(character_elem)
				script_elem.add_child(character_wrapper_elem)
				
				text_wrapper_elem = Util.new_node("span", story_html, class: "speech-wrapper")
				text_elem = Util.new_node("span", story_html, class: "speech")
				
				text_elem.add_child(text_content)
				text_wrapper_elem.add_child(text_elem)
				
				text_anchor_elem = Util.new_node("a", story_html,
					class: "speech-anchor",
					"aria-label": anchor_label,
					href: "##{text_id}"
				)
				text_wrapper_elem.add_child(text_anchor_elem)
				
				script_elem.add_child(text_wrapper_elem)
				is_cont = true
			end
		when "missionstatus"
			mission_status = script_xml["status"]
			mission_text = script_xml.content
			
			mission = LOCALIZATION.get("mission",
				capitalize: true,
				space_after: true
			)
			status = LOCALIZATION.get(mission_status)
			colon = LOCALIZATION.get("colon", space_after: true)
			open_quote = LOCALIZATION.get("open_quote")
			close_quote = LOCALIZATION.get("close_quote")
			
			mission_prompt_wrapper_elem = Util.new_node("span", story_html,
				class: "mission-prompt-wrapper"
			)
			mission_prompt_elem = Util.new_node("span", story_html, class: "mission-prompt")
			
			mission_status_elem = Util.new_node("span", story_html, class: "mission-status")
			mission_status_elem.content = mission + status
			mission_prompt_elem.add_child(mission_status_elem)
			
			unless mission_text.empty?
				mission_prompt_elem.add_child(colon)
				
				mission_text_elem = Util.new_node("span", story_html, class: "mission-text")
				mission_text_elem.content =
					open_quote +
					mission_text +
					close_quote
				
				mission_prompt_elem.add_child(mission_text_elem)
			end
			
			mission_prompt_wrapper_elem.add_child(mission_prompt_elem)
			script_elem.add_child(mission_prompt_wrapper_elem)
		else
			raise "unknown script xml! #{script_xml}"
		end
		
		parent.add_child(script_elem)
	end
end

base_xml.css("story > chapter").each do |chapter_xml|
	
	chapter_id = chapter_xml["id"]
	chapter_name = LOCALIZATION.get_subst(:chapters, chapter_id)
	
	chapter_elem = Util.new_node("article", story_html, class: "chapter") # TODO: not article?
	add_heading_with_anchor(
		chapter_elem,
		story_html,
		heading_type: "h2",
		content: chapter_name,
		anchor_id: chapter_id,
		anchor_type: "chapter"
	)
	
	chapter_xml.css("> episode, map").each do |episode_xml|
		
		episode_id = episode_xml["id"]
		episode_name = LOCALIZATION.get_subst(:episodes, episode_id)
		
		is_map = episode_xml.node_name == "map"
		episode_class = is_map ? "map" : "episode"
		
		episode_elem = Util.new_node("section", story_html, class: episode_class)
		add_heading_with_anchor(
			episode_elem,
			story_html,
			heading_type: "h3",
			content: episode_name,
			anchor_id: episode_id,
			anchor_type: "episode"
		)
		
		if is_map
			add_dialogue_elems(episode_elem, episode_xml, story_html)
		else
			episode_xml.css("> mission").each do |mission_xml|
				
				mission_name = mission_xml["name"] # no substitutions
				mission_id = mission_xml["id"]
				
				mission_localization = LOCALIZATION.get("mission",
					capitalize: true,
					space_after: true
				)
				
				mission_elem = Util.new_node("section", story_html, class: "mission")
				add_heading_with_anchor(
					mission_elem,
					story_html,
					heading_type: "h4",
					content: mission_localization + mission_name,
					anchor_id: mission_id,
					anchor_type: "mission"
				)
				
				mission_xml.css("> section").each do |section_xml|
					
					section_part = LOCALIZATION.get_subst(:sections, section_xml["status"])
					section_id = section_xml["id"]
					
					section_elem = Util.new_node("section", story_html, class: "section")
					add_heading_with_anchor(
						section_elem,
						story_html,
						heading_type: "h5",
						content: section_part,
						anchor_id: section_id,
						anchor_type: "section"
					)
					
					add_dialogue_elems(section_elem, section_xml, story_html)
					
					mission_elem.add_child(section_elem)
				end
				
				episode_elem.add_child(mission_elem)
			end
		end
		
		chapter_elem.add_child(episode_elem)
	end
	
	story_html.add_child(chapter_elem)
end

Util.write_html("story.html", LANG, story_html)
