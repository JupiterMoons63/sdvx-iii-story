#!/usr/bin/env ruby

require "./util"

content = File.read(ARGF.filename, encoding: "Shift_JIS")
content.encode!("UTF-8")

original_xml = Nokogiri::XML(content, nil, "UTF-8")
base_xml = Nokogiri::XML::Builder.new(encoding: "UTF-8") do |xml|
	xml.comment(
		"This file is not meant to be edited manually. Changes will be overwritten."
	)
	xml.story
end.to_xml
convert_xml = Nokogiri::XML(base_xml)
story_elem = convert_xml.at_css("story")

def get_id(elem)
	elem.css("> id").children.to_s
end

def split_script(script)
	script.children.to_s.split("\t").map(&:strip)
end

def chompify(text)
	text
		.delete_prefix('"')
		.chomp('"')
		.chomp("　")
		.chomp(" ")
end

def clean_text(text, line_breaks=true)
	text
		.delete_prefix('"')
		.gsub(/\R+/, line_breaks ? "<br/>" : " ")
		.gsub(/\u203e+/, "\u30fc")
		.chomp('"')
end

def clean_id(id, downcase=true)
	c_id = id
		.gsub(/\s+/, "")
		.gsub("17-20", "17_20")
		.gsub("21-23", "21_23")
	c_id.downcase! if downcase
	return c_id
end

def parse_chat(chat, id, convert_xml)
	scripts = chat.css("> script")
	
	return_elems = []
	
	prev_character_map = {}
	
	skip_next = false
	prev_in2 = false
	
	id_counter = 0
	
	until scripts.empty?
		script = scripts.shift
		
		if skip_next
			skip_next = false
			next
		end
		
		command, arg_1, arg_2, text = split_script(script)
		
		case command
		when "ms_mission", "system", "sound",
		     "music_start", "music_stop",
			 "preview_start", "preview_stop"
			# purely visual/audio effects that can't be put into writing
			# without hardcoding every instance
			next
		when "drama"
			cue_elem = Util.new_node("cue", convert_xml)
			
			cue_type =
				case arg_1
				when "in"
					"begin-dialogue"
				when "in2"
					prev_in2 = true
					"begin-monologue"
				when "out"
					if prev_in2
						prev_in2 = false
						"end-monologue"
					else
						"end-dialogue"
					end
				when "fadein"
					"bg-fade-in"
				when "fadeout"
					# check if the next command is a fadein
					script_next = scripts.first
					command_next, arg_1_next, arg_2_next, text_next = split_script(script_next)
					if command_next == "drama" && arg_1_next == "fadein"
						# skip the fadein
						skip_next = true
						# set the args to the next args
						arg_2 = arg_2_next
						text = text_next
						# use a unified cue
						"bg-fade-change"
					else
						"bg-fade-out"
					end
				when "slide"
					"bg-slide-change"
				when "move"
					# purely visual effect that clutters the script
					next
				else
					raise "unexpected drama arg! #{arg_1}"
				end
			
			cue_elem["type"] = cue_type
			
			cue_elem["bg"] = arg_2 unless arg_2.empty?
			
			location = chompify(text)
			cue_elem["location"] = location unless location.empty?
			
			return_elems << cue_elem
		when "drama_top", "drama_btm", "drama_big_text", "mission_message"
			location = {
				"drama_top" => "top",
				"drama_btm" => "bottom",
				"drama_big_text" => "text",
				"mission_message" => "mission",
			}[command]
			raise "impossible! #{command}" unless location
			
			if arg_1 == "out"
				cue_elem = Util.new_node("cue", convert_xml, type: "character-exit")
				character, _ = prev_character_map[location]
				cue_elem.content = character
				return_elems << cue_elem
			else
				id_counter += 1
				
				expression = arg_1
				character = arg_2
				
				character_elem = nil
				
				if character.empty?
					character, character_elem = prev_character_map[location]
				else
					character_elem = Util.new_node("character", convert_xml)
					prev_character_map[location] = [character, character_elem]
				end
				
				character_elem["name"] = character
				
				text_id = "#{id}-#{id_counter}"
				text_elem = Util.new_node("text", convert_xml, id: text_id)
				text_elem["expression"] = expression unless expression.empty?
				
				text = clean_text(text)
				text_elem.add_child(text)
				
				character_elem.add_child(text_elem)
				
				return_elems << character_elem
			end
		when "drama_big"
			case arg_1
				when "in"
					cue_elem = Util.new_node("cue", convert_xml,
						type: "begin-fullscreen",
						bg: arg_2
					)
					return_elems << cue_elem
				when "out"
					cue_elem = Util.new_node("cue", convert_xml, type: "end-fullscreen")
					return_elems << cue_elem
				when "text"
					id_counter += 1
					
					# no expression!
					
					character = arg_2
					
					character_elem = nil
					
					if character.empty?
						character, character_elem = prev_character_map[location]
					else
						character_elem = Util.new_node("character", convert_xml)
						prev_character_map[location] = [character, character_elem]
					end
					
					character_elem["name"] = character
					
					text_id = "#{id}-#{id_counter}"
					text_elem = Util.new_node("text", convert_xml, id: text_id)
					# TODO: expression = noface?
					
					text = clean_text(text)
					text_elem.add_child(text)
					
					character_elem.add_child(text_elem)
					
					return_elems << character_elem
				end
		when "mission_result"
			puts "warning: mission_result unimplemented"
			placeholder_elem = Util.new_node("placeholder", convert_xml)
			placeholder_elem.content = "mission_result unimplemented"
			return_elems << placeholder_elem
		when "mission_start", "mission_end"
			if arg_1 == "in" # we don't care about out
				status = command.delete_prefix("mission_")
				
				mission_status_elem = Util.new_node("missionstatus", convert_xml, status: status)
				
				mission_text = clean_text(text, line_breaks=false)
				mission_status_elem.content = mission_text unless mission_text.empty?
				
				return_elems << mission_status_elem
			end
		else
			raise "unexpected drama command! #{command}"
		end
		
	end
	
	return return_elems
end

CHAPTER_ID_MAP = { # for consistency's sake
	"プロローグ" => "c0",
	"第一話" => "c1",
	"第二話" => "c2",
	"SIDE01" => "cs1",
	"第三話" => "c3",
	"ここなつ01" => "ck1",
	"第四話" => "c4",
	"第五話" => "c5",
	"四天王＋六話" => "c6",
	"第七話" => "c7",
	"第八話" => "c8",
	"第九話" => "c9",
	"第十話" => "c10",
	"西日暮里2" => "cnn2",
	"裏1" => "cr01",
	"進化バトルレイシス" => "ce01",
	"第11話" => "c11",
	"第12話" => "c12",
	"第13話" => "c13",
	"第14話" => "c14",
	"第15話" => "c15",
	"第16〜20話" => "c16_20",
	"第21〜23話" => "c21_23",
	"第24話" => "c24"
}

original_xml.css("story > episode").drop(3).each do |chapter|
	
	chapter_id = CHAPTER_ID_MAP[get_id(chapter)]
	
	chapter_elem = Util.new_node("chapter", convert_xml, id: chapter_id)
	
	chats_by_episode =
		chapter
			.css("> chat")
			.group_by { |chat|
				chat_id = get_id(chat)
				if chat_id == "MAP_TUTORIAL"
					"Map Tutorial"
				elsif chat_id.include?("MAP")
					map_num = chat_id.split('_').first.to_i - 1
					map_text =
						case map_num
						when 17
							"17-20"
						when 21
							"21-23"
						else
							map_num.to_s
						end
					"Map #{map_text}"
				else
					chat_id.split(/[-]/).first
				end
			}
	
	chats_by_episode.each do |episode, chats|
		
		is_map = episode.include?("Map")
		
		episode_num = episode.to_i
		episode_text =
			if episode_num == 0
				episode
			else
				(episode_num - 1).to_s
			end
		episode_id = clean_id(is_map ? episode : "e#{episode_text}")
		
		node_type = is_map ? "map" : "episode"
		episode_elem = Util.new_node(node_type, convert_xml,
			name: episode_text,
			id: episode_id
		)
		
		chats_by_mission =
			chats
				.group_by { |chat|
					chat_id = get_id(chat)
					chat_id_segments = chat_id.split(/[-_]/)
					if chat_id.include?("MAP")
						"Map"
					elsif chat_id_segments.size == 3
						chat_id_segments[1]
					else
						"(None)"
					end
				}
		
		chats_by_mission.each do |mission, chats|
			mission_id = clean_id("#{episode_id}-m#{mission}")
			mission_elem = Util.new_node("mission", convert_xml,
				name: mission,
				id: mission_id
			)
			
			chats.each do |chat|
				
				section_segment =
					get_id(chat)
						.split(/[-_]/)
						.last
				section_id = clean_id("#{mission_id}-#{section_segment}")
				
				section_elem = Util.new_node("section", convert_xml,
					status: section_segment.downcase,
					id: section_id
				)
				
				parent_elem = is_map ? episode_elem : section_elem
				parent_id = is_map ? episode_id : section_id
				dialogue_elems = parse_chat(chat, parent_id, convert_xml)
				dialogue_elems.each do |dialogue_elem|
					parent_elem.add_child(dialogue_elem)
				end
				
				mission_elem.add_child(section_elem) unless is_map
			end
			
			episode_elem.add_child(mission_elem) unless is_map
		end
		
		chapter_elem.add_child(episode_elem)
	end
	
	story_elem.add_child(chapter_elem)
end

File.write("base.xml", Util.prettify_xml(convert_xml))
