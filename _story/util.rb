require "nokogiri"

class Util
	def self.parse_args(script_name)
		xml_file = ARGV[0]
		lang = ARGV[1]
		fallback_lang = ARGV[2]
		unless lang && fallback_lang && File.file?(xml_file)
			puts "usage: #{script_name}.rb <intermediary XML file> <target lang> <fallback lang>"
			exit 1
		end
		return [xml_file, lang, fallback_lang]
	end
	
	def self.read_xml(file_path)
		Nokogiri::XML(File.read(file_path))
	end
	
	def self.new_html_fragment
		Nokogiri::HTML5::DocumentFragment.parse("")
	end
	
	def self.new_node(type, doc, attributes={})
		node = Nokogiri::XML::Node.new(type, doc)
		attributes.each do |attribute, value|
			node[attribute] = value
		end
		return node
	end
	
	def self.all_keys_exist?(keys, hash)
		keys.all? { |key| hash[key] }
	end
	
	def self.write_out(file_name, lang, content)
		Dir.mkdir("out") unless Dir.exist?("out")
		Dir.mkdir("out/#{lang}") unless Dir.exist?("out/#{lang}")
		
		File.write("out/#{lang}/#{file_name}", content)
	end
	
	def self.write_html(file_name, lang, html)
		write_out(file_name, lang, prettify_xml(html))
	end
	
	def self.prettify_xml(xml)
		xml.to_xhtml(indent: 1, indent_text: "\t")
	end
end
