require "safe_yaml"
require "time"

# TODO: less bad

class TranslationLoader
	NATIVE_LANG = "jp"
	
	def initialize(lang)
		@LANG = lang
		@TRANSLATIONS = {}
		
		is_native = @LANG == NATIVE_LANG
		
		unless is_native
			dir = "translations/#{@LANG}/"
			raise "no translations directory for #{@LANG}" unless Dir.exist?(dir)
			
			translation_files = Dir.glob("#{dir}/*.yml")
			load_files(translation_files)
		end
	end
	
	private
	
	def load_files(files)
		files.each do |file_path|
			content = YAML.safe_load_file(file_path)
			file_name = File.basename(file_path)
			
			date = Time.parse(content["date"].to_s)
			translations = content["translations"]
			
			translations.each do |text_id, text|
				
				prev_translation = @TRANSLATIONS[text_id]
				
				if prev_translation
					prev_file_name = prev_translation[:file_name]
					prev_date = Time.parse(prev_translation[:date].to_s)
					prev_text = prev_translation[:text]
					
					prev_indent_size = prev_file_name.size
					indent_size = file_name.size
					warning_indent_size = 9
					warning_indent = ' ' * warning_indent_size
					
					prev_indented = indent_text(prev_text, prev_indent_size + warning_indent_size + 2)
					indented = indent_text(text, indent_size + warning_indent_size + 2)
					
					puts "warning: conflict for #{text_id} " \
					     "between #{prev_file_name} (#{prev_date}) " \
					     "and #{file_name} (#{date}). using newer translation..."
					puts warning_indent +
					     "#{prev_file_name}: #{prev_indented}"
					puts warning_indent +
					     "#{file_name}: #{indented}"
					
					if date == prev_date
						puts "fatal: conflicting dates match exactly, cannot make decision"
						exit 1
					elsif date > prev_date
						@TRANSLATIONS[text_id] = {
							file_name: file_name,
							date: date,
							text: text,
						}
					end
				else
					@TRANSLATIONS[text_id] = {
						file_name: file_name,
						date: date,
						text: text,
					}
				end
			end
		end
	end
	
	def indent_text(text, spaces)
		indent = ' ' * spaces
		text.strip.gsub(/\R+/, "\n" + indent)
	end
	
	def format_text(text)
		text.strip.gsub(/\R+/, "<br/>")
	end
	
	def sanitize_text(text)
		text.gsub('<', "&lt;").gsub('>', "&gt;")
	end
	
	public
	
	def get(text_id)
		translation = @TRANSLATIONS[text_id]
		return nil unless translation
		text = translation[:text]
		return nil unless text
		return format_text(sanitize_text(text))
	end
end
