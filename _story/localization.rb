require "safe_yaml"

class Localization
	NATIVE_LANG = "jp"
	
	@@warning_cache = Set.new
	
	def initialize(lang, fallback_lang)
		@LANG = lang
		@FALLBACK_LANG = fallback_lang
		@IS_NATIVE = lang == NATIVE_LANG
		
		@SUBST_TABLE =
			[:backgrounds, :sections, :expressions, :chapters, :episodes, :cues]
				.to_h { |table|
					[table, read_yaml("#{table}.yml")]
				}
		
		@CHARACTER_TABLE = read_yaml("characters.yml")
		@LOCALIZATION_TABLE = read_yaml("localization.yml")
		
		@SPACE = bootstrap("space")
	end
	
	private
	
	def read_yaml(file_path)
		YAML.safe_load_file("tables/#{file_path}", aliases: true)
	end
	
	def cached_warning(warning, text, table_name)
		cache_key = text + table_name.to_s
		unless @@warning_cache.include?(cache_key)
			@@warning_cache << cache_key
			puts warning
		end
	end
	
	def table_lookup(table, text, table_name)
		entry = nil
		begin
			entry = table[text][@LANG]
		rescue NoMethodError
			puts "fatal: missing entry for #{text} in #{table_name}"
			exit 1
		end
		unless entry
			entry = table[text][@FALLBACK_LANG]
			if entry
				cached_warning(
					"warning: no #{@LANG} entry for #{text} in #{table_name}",
					text,
					table_name
				)
			else
				puts "fatal: no #{@LANG} or #{@FALLBACK_LANG} entries for #{text} in #{table_name}"
				exit 1
			end
		end
		return entry
	end
	
	def bootstrap(text)
		table_lookup(@LOCALIZATION_TABLE, text, "localization")["text"]
	end
	
	def get_table_warnings(table, table_name)
		warnings = table.map do |text, langs|
			"no #{@LANG} entry for #{text} in #{table_name}" unless langs[@LANG]
		end
		return warnings.compact
	end
	
	def get_character_warnings(character_entry, name)
		warnings = []
		unless character_entry["name"][@LANG]
			warnings << "no #{@LANG} entry for #{name} name in characters"
		end
		unless !character_entry["variant"] || character_entry["variant"][@LANG]
			warnings << "no #{@LANG} entry for #{name} variant in characters"
		end
		return warnings
	end
	
	public
	
	def get_subst(table_name, text)
		table_lookup(@SUBST_TABLE[table_name], text, table_name)
	end
	
	def get_character(name)
		character_entry = @CHARACTER_TABLE[name]
		unless character_entry
			puts "fatal: missing entry for #{name} in characters"
			exit 1
		end
		character_name = table_lookup(character_entry, "name", "#{character_name} name")
		if character_entry["variant"]
			character_variant =
				table_lookup(character_entry, "variant", "#{character_name} variant")
		end
		if character_entry["tl_note"] && !@IS_NATIVE
			character_tl_note =
				table_lookup(character_entry, "tl_note", "#{character_name} tl_note")
		end
		return {
			name: character_name,
			variant: character_variant,
			tl_note: character_tl_note
		}
	end
	
	def get(text, options={})
		entry = table_lookup(@LOCALIZATION_TABLE, text, "localization")
		
		do_capitalize = options[:capitalize] && entry["can_capitalize"]
		do_space_before = options[:space_before] && entry["can_space_before"]
		do_space_after = options[:space_after] && entry["can_space_after"]
		
		entry_text = entry["text"]
		unless entry_text
			puts "fatal: localization for #{text} missing text"
			exit 1
		end
		entry_text.capitalize! if do_capitalize
		
		space_before = do_space_before ? @SPACE : ""
		space_after  = do_space_after  ? @SPACE : ""
		
		if options[:manual_spaces]
			return {
				text: entry_text,
				space_before: space_before,
				space_after: space_after
			}
		else
			return space_before + entry_text + space_after
		end
	end
	
	def get_warnings
		warnings = []
		@SUBST_TABLE.each do |table_name, table|
			warnings += get_table_warnings(table, table_name)
		end
		warnings += get_table_warnings(@LOCALIZATION_TABLE, "localization")
		@CHARACTER_TABLE.each do |character, entry|
			warnings += get_character_warnings(entry, character)
		end
		return warnings
	end
end
